﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public sealed class SqlData
{
    public static string nserie;
    public static string userid;
    public static string idioma;
    public static string db;
    public static string ip = Utils.IpAddress();

    static string connectionString;
    static SqlConnection connection;
    static SqlCommand command;

    static DataTable dt_TRADUCOES = new DataTable();



    public static void setConnString()
    {

        try
        {
            connectionString = "Data Source=89.26.246.9;Initial Catalog=CASAEXPRESS;Persist Security Info=True;User ID=inet;Password=ix81md#;MultipleActiveResultSets=True";
            SqlConnection connection = new SqlConnection(connectionString);
            command = connection.CreateCommand();
            command.Connection.Open();
        }
        catch (Exception ex) { Console.WriteLine(ex); }

        DataTable dt = new DataTable();
        string query = "SELECT db from CLIENTES_XIMO where ximo_nserie='" + nserie + "'";
        dt.Load(SQL_OPEN(query));
        dt.Dispose();
        if (dt.Rows.Count > 0)
        {
            db = dt.Rows[0]["db"].ToString();

        }
        ///* TESTE */
        db = "XIMO_AUX";
        connectionString = "Data Source=89.26.246.9;Initial Catalog="+ db + ";Persist Security Info=True;User ID=inet;Password=ix81md#;MultipleActiveResultSets=True";
        //connectionString = "Data Source=localhost;Initial Catalog=" + db + ";Persist Security Info=True;User ID=sa;Password=password;MultipleActiveResultSets=True";
    }



    public static SqlDataReader SQL_OPEN(string Query)
    {
        SqlConnection con = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();

        try
        {
            command.Connection = con;
            command.CommandTimeout = 120;
            command.Connection.Open();
            command.CommandText = Query;
        }
        catch (SqlException ex)
        {
            Console.WriteLine(ex);
        }

        return command.ExecuteReader(CommandBehavior.CloseConnection);
    }

    public static void SQL_EXEC(string Query)
    {
        SqlConnection con = new SqlConnection(connectionString);
        SqlCommand command = new SqlCommand();

        try
        {
            command.Connection = con;
            command.CommandTimeout = 3600;
            command.Connection.Open();
            command.CommandText = Query;
            command.ExecuteReader(CommandBehavior.CloseConnection).Close();

        }
        catch (SqlException ex)
        {
            Console.WriteLine(ex);
        }
    }

    public static string traducao(string variavel, string idioma)
    {
        string texto = "";

        if(dt_TRADUCOES.Rows.Count==0)
           dt_TRADUCOES.Load(SqlData.SQL_OPEN("SELECT variavel,texto_pt FROM XIMO_VARIAVEIS WHERE marketplace=1"));

        DataRow[] rows = dt_TRADUCOES.Select("variavel='" + variavel + "'", "");
        if (rows.Count() > 0)
            texto = rows[0]["texto_" + idioma].ToString();
        else
        {
            texto = "falta tradução";
        }

        dt_TRADUCOES.Dispose();
        return texto;
    }

    //public static string traducao(string variavel, string idioma)
    //{
    //    string texto = "";
    //    DataTable dt = new DataTable();
    //    dt.Load(SqlData.SQL_OPEN("SELECT texto_pt FROM XIMO_AUX.dbo.XIMO_VARIAVEIS where texto_pt like '%Subscricoes%' AND)");
    //    DataRow[] rows = dt.Select("variavel='" + variavel + "'", "");
    //    if (rows.Count() > 0)
    //        texto = rows[0]["texto_" + idioma].ToString();
    //    return texto;
    //}


   
}
    
