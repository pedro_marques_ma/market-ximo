﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

public sealed class ArtigosData
{

    public static List<Artigo> lista = null;

    public static List<Artigo> ListarArtigos(string liTipo, string idioma, string rowsPage, string pagina, string ordenacao, string pesquisa, string estado)
    {

        DataTable dt = new DataTable();
        dt.Load(SqlData.SQL_OPEN("EXEC sp_XIMOMP_Artigos_Listar @tipoList='" + liTipo + "', @idioma= '" + idioma + "', @rowsPage = '" + rowsPage + "',@page = '" + pagina + "',@ordem = '" + ordenacao + "',@pesquisa = '" + pesquisa + "', @activo = '" + estado + "',@nserie = '" + SqlData.nserie + "';"));
        dt.Dispose();
        if (dt.Rows.Count > 0)
        {
            lista = new List<Artigo>();
            foreach (DataRow row in dt.Rows)
            {
                Artigo obj = new Artigo();
                obj.Id = row["Id"].ToString();
                obj.categoriaId = row["idCategoria"].ToString();
                obj.categoriaNome = row["CategoriaNome"].ToString();
                obj.descricao = row["descricao_pt"].ToString();
                obj.subtitulo = row["subtitulo_pt"].ToString();
                obj.titulo = row["titulo_pt"].ToString();
                obj.preco = decimal.Parse(row["preco"].ToString());
                obj.descontoPerc = decimal.Parse(row["descontoPerc"].ToString());
                obj.periocidade = int.Parse(row["periocidade"].ToString());
                obj.destaque = int.Parse(row["destaque"].ToString());

                if (row["existe"].ToString() == "")
                    obj.existeCarrinho = 0;
                else
                    obj.existeCarrinho = 1;

                obj.fotos = new List<Artigo_fotos>();
                DataTable dtfotos = new DataTable();
                dtfotos.Load(SqlData.SQL_OPEN("SELECT * FROM XIMO_MARKETPLACE_ARTIGOS_FOTOS WHERE idArtigo= '" + obj.Id + "' and tipo=1;"));
                dtfotos.Dispose();

                lista.Add(obj);
            }
        }

        return lista;
    }
}
