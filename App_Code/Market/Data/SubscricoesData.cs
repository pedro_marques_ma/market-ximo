﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Xml;

public sealed class SubscricoesData
{

    public static Carrinho carrinho = null;

    public List<Subscricao> ListarSubscricao(string idioma, string rowsPage, string pagina, string ordenacao, string pesquisa, string estado)
    {
        List<Subscricao> lista = new List<Subscricao>();
        DataTable dt = new DataTable();
        dt.Load(SqlData.SQL_OPEN("EXEC sp_XIMOMP_Subscricao_List @tipoList=1, @idioma= " + idioma + ", @rowsPage = " + rowsPage + ",@page = " + pagina + ",@Ordenacao = '" + ordenacao + "',@Pesquisa = '" + pesquisa + "', @Estado = '" + estado + "';"));
        dt.Dispose();
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow row in dt.Rows)
            {
                Subscricao obj = new Subscricao();
                obj.Id = row["Id"].ToString();
                obj.nserie = row["nserie"].ToString();
                obj.entidade = row["entidade"].ToString();
                obj.referencia = row["referencia"].ToString();
                obj.pago = bool.Parse(row["pago"].ToString());
                obj.total = decimal.Parse(row["total"].ToString());
                obj.inseridoUser = row["UserId"].ToString();
                obj.inseridoData = DateTime.Parse(row["inseridoData"].ToString());
                obj.anulado = bool.Parse(row["anulado"].ToString());
                obj.anuladoData = DateTime.Parse(row["anuladoData"].ToString());
                obj.anuladoMotivo = row["anuladoMotivo"].ToString();
               

                obj.linhas = new List<Subscricao_Linhas>();
                DataTable dtlinhas = new DataTable();
                dtlinhas.Load(SqlData.SQL_OPEN("EXEC sp_Subscricao_LinhasList @idArtigo= " + obj.Id + "';"));
                dtlinhas.Dispose();
            }
        }
        return lista;
    }

    //public List<Artigo> EditarCarrinho(string nserie, string idioma, string rowsPage, string pagina, string ordenacao, string pesquisa, string estado)
    //{
    //    List<Artigo> lista = new List<Artigo>();
    //    DataTable dt = new DataTable();
    //    dt.Load(SqlData.SQL_OPEN("EXEC sp_XIMOMP_Subscricao_List @tipoList=1, @idioma= " + idioma + ", @rowsPage = " + rowsPage + ",@page = " + pagina + ",@Ordenacao = '" + ordenacao + "',@Pesquisa = '" + pesquisa + "', @Estado = '" + estado + "';"));
    //    dt.Dispose();
    //    if (dt.Rows.Count > 0)
    //    {
    //        foreach (DataRow row in dt.Rows)
    //        {
    //            Subscricao obj = new Subscricao();
    //            obj.Id = row["Id"].ToString();
    //            obj.nserie = row["nserie"].ToString();
    //            obj.entidade = row["entidade"].ToString();
    //            obj.referencia = row["referencia"].ToString();
    //            obj.pago = bool.Parse(row["pago"].ToString());
    //            obj.total = decimal.Parse(row["total"].ToString());
    //            obj.inseridoUser = row["UserId"].ToString();
    //            obj.inseridoData = DateTime.Parse(row["inseridoData"].ToString());
    //            obj.anulado = bool.Parse(row["anulado"].ToString());
    //            obj.anuladoData = DateTime.Parse(row["anuladoData"].ToString());
    //            obj.anuladoMotivo = row["anuladoMotivo"].ToString();

    //            obj.linhas = new List<Subscricao_Linhas>();
    //            DataTable dtlinhas = new DataTable();
    //            dtlinhas.Load(SqlData.SQL_OPEN("EXEC sp_Subscricao_LinhasList @idArtigo= " + obj.Id + "';"));
    //            dtlinhas.Dispose();
    //        }
    //    }
    //    return lista;
    //}

    public static string InserirSubscricao(string nserie, string entidade, string referencia, string total,string referencialink, List<Artigo> listaSelecionados)
    {
        string newId = "";
        DataTable dt = new DataTable();
        dt.Load(SqlData.SQL_OPEN("EXEC sp_XIMOMP_Subscricao_Inserir @nserie='" + nserie + "',@entidade='" + entidade + "',@referencia='" + referencia + "',@total='" + total + "',@link='" + referencialink + "',@insert_user='" + SqlData.userid + "',@ip='" + SqlData.ip + "';"));
        dt.Dispose();
        if (dt.Rows.Count > 0)
            newId = dt.Rows[0]["id"].ToString();

        int newLinhaId = 1;
        foreach (Artigo obj in listaSelecionados)
        {
            
            DataTable dtLinhas = new DataTable();
            dtLinhas.Load(SqlData.SQL_OPEN("EXEC sp_XIMOMP_SubscricaoLinhas_Inserir @nserie='" + nserie + "',@id='" + newLinhaId + "',@SubscricaoId='" + newId + "',@ArtigoId='" + obj.Id + "',@preco ='" + obj.preco + "',@preco_aplicar ='" + (obj.preco * (obj.descontoPerc / 100)) + "',@periocidade='" + obj.periocidade + "',@insert_user='" + SqlData.userid + "',@ip='" + SqlData.ip + "';"));
            dtLinhas.Dispose();
            if (dtLinhas.Rows.Count > 0)
            {
                dtLinhas.Rows[0]["id"].ToString();
                newLinhaId++;
            }
        }

        

        return newId;
    }

    public static string EditarSubscricao(string id, string nserie, string entidade, string referencia, string total, List<Subscricao_Linhas> linhas, string userid, string ip)
    {
        string editId = "";
        DataTable dt = new DataTable();
        dt.Dispose();
        dt.Load(SqlData.SQL_OPEN("EXEC sp_XIMOMP_Subscricao_Editar @Id='" + id + "',@nserie='" + nserie + "',@entidade='" + entidade + "',@referencia='" + referencia + "',@total='" + total + "',@insert_user='" + userid + "',@insert_ip='" + ip + "';"));

        if (dt.Rows.Count > 0)
            editId = dt.Rows[0]["id"].ToString();

        return editId;
    }

    public static Carrinho EditarCarrinho(string nserie,string userid,string idioma, string rowsPage, string pagina, string ordenacao, string pesquisa)
    {
        carrinho.listaSelecionados = new List<Artigo>();
        DataTable dt = new DataTable();
        dt.Dispose();
        dt.Load(SqlData.SQL_OPEN("EXEC sp_XIMOMP_Carrinho_Listar @nserie='" + nserie + "',@userid='" + userid + "',@idioma='" + idioma + "', @rowsPage = '" + rowsPage + "',@page = '" + pagina + "',@ordem = '" + ordenacao + "',@pesquisa = '" + pesquisa + "';"));
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow row in dt.Rows)
            {
                Artigo obj = new Artigo();
                obj.Id = row["idArtigo"].ToString();
                obj.categoriaId = row["idCategoria"].ToString();               
                obj.subtitulo = row["subtitulo_pt"].ToString();
                obj.titulo = row["titulo_pt"].ToString();
                obj.preco = decimal.Parse(row["preco"].ToString());
                obj.periocidade = int.Parse(row["periocidade"].ToString());
                obj.descontoPerc = int.Parse(row["descontoPerc"].ToString());

                obj.fotos = new List<Artigo_fotos>();
                DataTable dtfotos = new DataTable();
                dtfotos.Load(SqlData.SQL_OPEN("SELECT * FROM XIMO_MARKETPLACE_ARTIGOS_FOTOS WHERE idArtigo= '" + obj.Id + "' and tipo=2;"));
                dtfotos.Dispose();
                             
                carrinho.listaSelecionados.Add(obj);
            }
        }
        return carrinho;
    }
  
    public static string InserirCarrinhoItem(string artigoId)
    {
        string newId = "";
        DataTable dt = new DataTable();
        dt.Load(SqlData.SQL_OPEN("EXEC sp_XIMOMP_Carrinho_Inserir @nserie='" + SqlData.nserie + "',@artigoId='" + artigoId + "',@insert_user='" + SqlData.userid + "',@ip='" + SqlData.ip + "';"));
        dt.Dispose();
        if (dt.Rows.Count > 0)
            newId = dt.Rows[0]["id"].ToString();

        return newId;
    }

    public static string UpdateCarrinho(string id, string nserie, string artigoId, string preco, string preco_aplicar, string userid, string ip)
    {
        string editId = "";
        DataTable dt = new DataTable();
        dt.Dispose();
        dt.Load(SqlData.SQL_OPEN("EXEC sp_XIMOMP_Carrinho_Update @Id='" + id + "',@nserie='" + nserie + "',@artigoId='" + artigoId + "',@preco='" + preco + "',@preco_aplicar='" + preco_aplicar + "',@insert_user='" + userid + "',@ip='" + ip + "';"));

        if (dt.Rows.Count > 0)
            editId = dt.Rows[0]["id"].ToString();

        return editId;
    }

    public static string EliminarItemsCarrinho(string artigoId,string deleteAll)
    {
        string id = "";
        DataTable dt = new DataTable();
        dt.Dispose();
        dt.Load(SqlData.SQL_OPEN("EXEC sp_XIMOMP_Carrinho_Eliminar @nserie='" + SqlData.nserie + "',@ArtigoId='" + artigoId + "',@deleteAll='" + deleteAll + "',@userid='" + SqlData.userid + "',@ip='" + SqlData.ip + "';"));

        if (dt.Rows.Count > 0)
            id = dt.Rows[0]["id"].ToString();

        return id;
    }


    /* REFERENCIAS EASYPAY */
    public static DataTable GerarRefMultibanco(string ValorPagamento)
    {
        DataTable dt = new DataTable();
        Easypay_wrapper ep = new Easypay_wrapper();

        try
        {
            ep.Username = ConfigurationManager.AppSettings["EasypayClientID"].ToString();
            ep.CIN = int.Parse(ConfigurationManager.AppSettings["EasypayCIN"].ToString());
            ep.Entity = int.Parse(ConfigurationManager.AppSettings["EasypayEntity"].ToString());

            XmlDocument reference = ep.CreateReference("1", ValorPagamento);

            dt.Columns.Add("Entidade", Type.GetType("System.String"));
            dt.Columns.Add("Referencia", Type.GetType("System.String"));
            dt.Columns.Add("Valor", Type.GetType("System.String"));
            dt.Columns.Add("Link", Type.GetType("System.String"));

            if (reference.SelectSingleNode("getautoMB/ep_status").InnerText.ToString() == "ok0")
            {
                DataRow row = dt.NewRow();
                row["Entidade"] = reference.SelectSingleNode("getautoMB/ep_entity").InnerText.ToString();
                row["Referencia"] = reference.SelectSingleNode("getautoMB/ep_reference").InnerText.ToString();
                row["Valor"] = reference.SelectSingleNode("getautoMB/ep_value").InnerText.ToString();
                row["Link"] = reference.SelectSingleNode("getautoMB/ep_link").InnerText.ToString();
                dt.Rows.Add(row);

            }

        }
        catch (Exception ex)
        {
            Utils.SendEmail("pedromarques@grupoma.eu", "Erro - gerefs: ", ex.Message + " EasypayUser: " + ep.Username + " EasypayCIN:" + ep.CIN + " EasypayENTI:" + ep.Entity + " valor:" + ValorPagamento);
            return null;
        }

        return dt;
    }

}


   