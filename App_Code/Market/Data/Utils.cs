﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


public sealed class Utils
{

    private Utils()
    {

    }

    #region Easypay

    public static string EasypayClientID()
    {
        return ConfigurationManager.AppSettings["EasypayClientID"];
    }

    public static string EasypayPassword()
    {
        return ConfigurationManager.AppSettings["EasypayPassword"];
    }

    public static string EasypayCIN()
    {
        return ConfigurationManager.AppSettings["EasypayCIN"];
    }

    public static string EasypayEntity()
    {
        return ConfigurationManager.AppSettings["EasypayEntity"];
    }

    #endregion

    #region Funções Utils
    public static string IpAddress()
    {
        HttpRequest Req = HttpContext.Current.Request;
        string strIpAddress;
        strIpAddress = Req.ServerVariables["HTTP_X_FORWARDED_FOR"];
        if (strIpAddress == null)
        {
            strIpAddress = Req.ServerVariables["REMOTE_ADDR"];
        }
        return strIpAddress;
    }

    public static string formatPreco(string preco)
    {
        Double num = 0;
        if (!Double.TryParse(preco, out num))
            preco = "0";
        Double precoD = Convert.ToDouble(preco);
        if (preco.IndexOf(",") != -1)
            return formatPreco(precoD, 1);
        else
            return formatPreco(precoD, 0);
    }

    public static string formatPreco(string preco, int numdec)
    {
        Double num = 0;
        if (!Double.TryParse(preco, out num))
            preco = "0";
        Double precoD = Convert.ToDouble(preco);
        return formatPreco(precoD, numdec);
    }

    public static string formatPreco(double preco)
    {
        return formatPreco(preco, 0);
    }

    public static string formatPreco(double preco, int numdec)
    {
        return String.Format(getCultureInfoIdioma(), "{0:0,0.00}", preco.ToString("c" + numdec, getCultureInfoIdioma())).Replace("€", "").Replace("$", "").Trim();
    }

    public static CultureInfo getCultureInfoIdioma()
    {
        string idioma = "pt";
        CultureInfo ci = null;
        if (idioma == "pt") // Português
            ci = new CultureInfo("pt-PT");
        else if (idioma == "en") //Inglês
            ci = new CultureInfo("en-US");
        else if (idioma == "fr") //Francês
            ci = new CultureInfo("fr-FR");
        else if (idioma == "de") //Alemão
            ci = new CultureInfo("de-DE");
        else if (idioma == "es") //Espanhol
            ci = new CultureInfo("es-ES");
        else if (idioma == "it") //Italiano
            ci = new CultureInfo("it-IT");
        return ci;
    }

    public static void copyAll(string SourcePath, string DestinationPath, bool deleteSourceFiles)
    {
        if (!Directory.Exists(DestinationPath))
            Directory.CreateDirectory(DestinationPath);

        //Now Create all of the directories
        if (Directory.Exists(SourcePath))
        {
            foreach (string dirPath in Directory.GetDirectories(SourcePath, "*", SearchOption.AllDirectories))
                Directory.CreateDirectory(DestinationPath + dirPath.Remove(0, SourcePath.Length));

            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(SourcePath, "*.*", SearchOption.AllDirectories))
                File.Copy(newPath, DestinationPath + newPath.Remove(0, SourcePath.Length), true);

            if (deleteSourceFiles)
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(SourcePath);

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
            }
        }
    }

    public static string FirstCharToUpper(string input)
    {
        if (String.IsNullOrEmpty(input))
            throw new ArgumentException("");
        return input.First().ToString().ToUpper() + String.Join("", input.Skip(1));
    }

    public static string Ds(String txt)
    {
        return txt.Replace("\r\n", "\n").Replace("\r", "\n").Replace("\n", "\r\n").Replace(Environment.NewLine, "<br />");
    }

    private static string HtmlToPlainText(string html)
    {
        const string tagWhiteSpace = @"(>|$)(\W|\n|\r)+<";//matches one or more (white space or line breaks) between '>' and '<'
        const string stripFormatting = @"<[^>]*(>|$)";//match any character between '<' and '>', even when end tag is missing
        const string lineBreak = @"<(br|BR|TR|tr)\s{0,1}\/{0,1}>";//matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />,<TR>
        const string tdSpace = @"<td(.|\n)*?>";//matches: <td>
        const string styles = @"<style(.|\n)*?</style>";//matches: <style>...</style>
        const string scripts = @"<script(.|\n)*?</script>";//matches: <script>...</script>

        var stylesRegex = new Regex(styles, RegexOptions.Multiline);
        var scriptsRegex = new Regex(scripts, RegexOptions.Multiline);
        var lineBreakRegex = new Regex(lineBreak, RegexOptions.Multiline);
        var tdSpaceRegex = new Regex(tdSpace, RegexOptions.Multiline);
        var stripFormattingRegex = new Regex(stripFormatting, RegexOptions.Multiline);
        var tagWhiteSpaceRegex = new Regex(tagWhiteSpace, RegexOptions.Multiline);

        var text = html;

        //Decode html specific characters
        text = System.Net.WebUtility.HtmlDecode(text);
        //Remove tag whitespace/line breaks
        text = tagWhiteSpaceRegex.Replace(text, "><");
        //Replace <br /> with line breaks
        text = tdSpaceRegex.Replace(text, "       ");
        //Replace <td> with spaces
        text = lineBreakRegex.Replace(text, Environment.NewLine);
        //replace scripts
        text = scriptsRegex.Replace(text, string.Empty);
        //replace styles
        text = stylesRegex.Replace(text, string.Empty);
        //Strip formatting
        text = stripFormattingRegex.Replace(text, string.Empty);
        //trimleft
        text = string.Join(
                             "\n",
                             text.Split('\n').Select(s => s.Trim()));
        return text;
    }

    public class HttpResponseInfo
    {
        public HttpResponseInfo(HttpStatusCode statusCode, string text)
        {
            StatusCode = statusCode;
            Text = text;
        }

        public HttpStatusCode StatusCode { get; set; }
        public string Text { get; set; }

        public static HttpResponseInfo New(HttpStatusCode statusCode, string text)
        {
            return new HttpResponseInfo(statusCode, text);
        }
    }

    #endregion Funções Utils

    #region Class Body
    public static string ClassBodyGet()
    {
        System.Web.SessionState.HttpSessionState Session = HttpContext.Current.Session;
        if (Session["userMenuExpand"].ToString() == "1")
            return "";
        else
            return "mini-navbar";
    }
    #endregion Class Body

    #region Rating

    public static void getLiteralRating(System.Web.UI.WebControls.Literal lit, double Rating)
    {
        lit.Text += getHTMLRating(Rating);
    }

    public static string getHTMLRating(double Rating)
    {
        string html = "";
        if (Rating > 0)
        {
            int count = 0;
            for (int i = 0; i < (int)Rating / 1; i++)
            {
                html += "<div class='rt rt100'></div>";
                count++;
            }
            if (Rating % 1 != 0)
            {
                html += "<div class='rt rt50'></div>";
                count++;
            }
            if (count < 5)
            {
                for (int i = count; i < 5; i++)
                {
                    html += "<div class='rt rt0'></div>";
                }
            }
        }
        else
        {
            for (int i = 0; i < 5; i++)
            {
                html += "<div class='rt rt0'></div>";
            }
        }
        return html;
    }

    #endregion Rating

    #region Send Emails

    public enum MessageTemplate
    {
        TemplateSimples,
        Emailpagamentos,
        TemplateSam
    }

    public static bool SendEmail(MessageTemplate messageTemplate, ListDictionary replacements, string emailTo, string Subject)
    {
        var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
        MailMessage mailMessage = CreateMessage(messageTemplate, replacements, emailTo, smtpSection.From, null, Subject);
        return InternalSend(mailMessage, false);
    }

    public static bool SendEmail(MessageTemplate messageTemplate, ListDictionary replacements, string emailTo, string cc, string Subject)
    {
        var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
        MailMessage mailMessage = CreateMessage(messageTemplate, replacements, emailTo, smtpSection.From, cc, Subject);
        return InternalSend(mailMessage, false);
    }

    public static bool SendMessage(MessageTemplate messageTemplate, ListDictionary replacements, string to, string from, string subject)
    {
        MailMessage mailMessage = CreateMessage(messageTemplate, replacements, to, from, null, subject);
        return InternalSend(mailMessage, false);
    }

    public static System.Net.Mail.MailMessage CreateMessage(MessageTemplate messageTemplate, ListDictionary replacements, string to, string from, string cc, string subject)
    {
        string physicalPath = HttpContext.Current.Server.MapPath("~/App_Data/Templates/" + messageTemplate.ToString() + ".htm");

        if (!File.Exists(physicalPath))
        {
            throw new ArgumentException("Invalid MailTemplate Passed into the NotificationManager", "messageTemplate");
        }

        MailDefinition md = new MailDefinition();
        md.BodyFileName = physicalPath;
        md.Subject = subject;
        md.From = from;
        md.CC = cc;
        md.IsBodyHtml = true;

        System.Net.Mail.MailMessage mailMessage = md.CreateMailMessage(to, replacements, new Button());
        return mailMessage;
    }

    private static bool InternalSend(MailMessage mailMessage, bool isAsync)
    {
        try
        {
            if (!isAsync)  // blocks current thread
                MailSender.SendMailMessage(mailMessage);
            else  // does not block current thread
                MailSender.SendMailMessageAsync(mailMessage);
        }
        catch (Exception ex)
        {
            //handle, log, do something
            throw;
        }
        return true;
    }

    public static bool SendEmail(string email, string Subject, string Body)
    {
        var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

        try
        {
            MailMessage mail = new MailMessage();
            mail.To.Add(email);
            mail.From = new MailAddress(smtpSection.From);
            mail.Subject = Subject;
            mail.Body = Body;
            mail.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = smtpSection.Network.Host;
            smtp.Port = smtpSection.Network.Port;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential(smtpSection.Network.UserName, smtpSection.Network.Password);
            smtp.EnableSsl = false;
            smtp.Send(mail);
            return true;
        }
        catch (Exception ex)
        {
            Utils.SendEmail("joaopereira@grupoma.eu,pedromarques@grupoma.eu", "Erro Email" + email, ex.Message);
            Console.Write("Could not send the e-mail - error: " + ex.Message);
            return false;
        }
    }

    public static bool SendMessageMailList(MessageTemplate messageTemplate, ListDictionary replacements, string to, string subject, string idUser, string IP)
    {
        return SendMessageMailList(messageTemplate, replacements, to, "", subject, idUser, IP);
    }

    public static bool SendMessageMailList(MessageTemplate messageTemplate, ListDictionary replacements, string to, string from, string subject, string idUser, string IP)
    {
        string bodyHtml = getMessageBody(messageTemplate, replacements, to, subject);
        bodyHtml = bodyHtml.Replace("'", "\"");
        string bodyText = HtmlToPlainText(bodyHtml);
        if (from.Trim() == "")
        {
            var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
            from = smtpSection.From;
        }

        try
        {
            SqlData.SQL_EXEC("EXEC sp_SendHTMLEmail_MailListInsert @To='" + to + "',@From='" + from + "',@Assunto='" + subject + "',@BodyHTML='" + bodyHtml + "',@BodyText='" + bodyText + "',@AngariadorId='" + idUser + "',@IP='" + IP + "'");
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
        return false;
    }

    public static string getMessageBody(MessageTemplate messageTemplate, ListDictionary replacements, string to, string subject)
    {
        MailMessage mailMessage = CreateMessage(messageTemplate, replacements, to, null, null, subject);
        return mailMessage.Body;
    }

    public class MailSender
    {
        public static string mailStatus = String.Empty;
        public static event EventHandler NotifyCaller;
        protected static void OnNotifyCaller()
        {
            if (NotifyCaller != null) NotifyCaller(mailStatus, EventArgs.Empty);
        }

        public static bool SendMailMessage(MailMessage mailMessage)
        {
            try
            {
                using (SmtpClient client = new SmtpClient())
                {
                    var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
                    client.Credentials = new System.Net.NetworkCredential(smtpSection.Network.UserName, smtpSection.Network.Password);
                    client.Send(mailMessage);
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
                throw;
            }
            return true;
        }

        public static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            String token = (string)e.UserState;
            if (e.Cancelled)
            {
                mailStatus = token + " Send canceled.";
            }
            if (e.Error != null)
            {
                mailStatus = "Error on " + token + ": " + e.Error.ToString();
            }
            else
            {
                mailStatus = token + " mail sent.";
            }
            OnNotifyCaller();
        }

        public static void SendMailMessageAsync(MailMessage mailMessage)
        {
            try
            {
                SmtpClient client = new SmtpClient();
                var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
                client.Credentials = new System.Net.NetworkCredential(smtpSection.Network.UserName, smtpSection.Network.Password);
                client.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
                client.SendAsync(mailMessage, mailMessage.To.ToString());
            }
            catch (Exception)
            {
                //Log
                throw;
            }
        }
    }


    #endregion Send Emails

   

}



