﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public sealed class Artigo
{
    public string Id { get; set; }

        public string categoriaId { get; set; }

        public string categoriaNome { get; set; }

        public string data_lancamento { get; set; }

        public int destaque { get; set; }

        public decimal preco { get; set; }

        public decimal descontoPerc { get; set; }

        public int periocidade { get; set; }

        public string titulo { get; set; }

        public string subtitulo { get; set; }

        public string descricao { get; set; }

        public List<Artigo_fotos> fotos { get; set; }

        public List<Artigo> lista { get; set; }

        public int existeCarrinho { get; set; }
}

    public class Artigo_fotos
    {
        public string Id { get; set; }

        public string ArtigoId { get; set; }

        public string foto { get; set; }

        public string principal { get; set; }

        public bool visivel { get; set; }

    }

    
