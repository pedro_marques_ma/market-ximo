﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public sealed class Subscricao
{
    public string Id { get; set; }

    public string nserie { get; set; }

    public string referencia { get; set; }

    public decimal total { get; set; }

    public string entidade { get; set; }

    public bool pago { get; set; }

    public DateTime pago_data { get; set; }

    public bool anulado { get; set; }

    public DateTime inseridoData { get; set; }

    public DateTime anuladoData { get; set; }

    public string anuladoMotivo { get; set; }

    public string inseridoUser { get; set; }

    public string inseridoIp { get; set; }

    public List<Subscricao_Linhas> linhas { get; set; }
}

public class Subscricao_Linhas
{
    public string Id { get; set; }

    public string SubscricaoId { get; set; }

    public string ArtigoId { get; set; }

    public decimal preco { get; set; }

    public decimal preco_aplicar { get; set; }

    public int periocidade { get; set; }

}

public class Carrinho
{
    public string Id { get; set; }

    public string nserie { get; set; }

    public List<Artigo> listaSelecionados { get; set; }

    public decimal descontoPerc { get; set; }

    public int total_items { get; set; }

    public decimal total { get; set; }

}

