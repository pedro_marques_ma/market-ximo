﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml;

public class Easypay_wrapper
{
    //Username used to connect through easypay api
    public string Username
    {
        get { return strUsername; }
        set { strUsername = value; }
    }
    private string strUsername;

    //Entity used to connect through easypay api
    public int Entity
    {
        get { return strEntity; }
        set { strEntity = value; }
    }
    private int strEntity;

    //CIN used to connect through easypay api
    public int CIN
    {
        get { return intCIN; }
        set { intCIN = value; }
    }
    private int intCIN;

    //Country used to connect through easypay api
    public string Country
    {
        get { return strCountry; }
        set { strCountry = value; }
    }
    private string strCountry = "PT";

    //Language used to connect through easypay api
    public string Language
    {
        get { return strLanguage; }
        set { strLanguage = value; }
    }
    private string strLanguage = "PT";

    public string Server_Production
    {
        get { return "https://www.easypay.pt/_s/"; }
    }

    //Sets if the connection is production or test
    public bool isLive
    {
        get { return bLive; }
        set { bLive = value; }
    }
    private bool bLive = false;

    //Enum to the existing APIs
    public enum api
    {
        api_easypay_01BG,
        api_easypay_05AG,
        api_easypay_03BG,
        api_easypay_040BG1,
        api_easypay_07BG,
        api_easypay_23AG,

        api_easypay_03AG
    }

    //Enum to the Reference Type
    public enum ReferenceType
    {
        normal,
        boleto,
        recurring,
        moto

    }

    //Enum to the Payment Type
    public enum PaymentType
    {
        creditcard,
        recurring
    }

    /// <summary>
    /// Creates a new reference.
    /// </summary>
    /// <returns>
    /// The reference.
    /// </returns>
    /// <param name='t_key'>
    /// T_key.
    /// </param>
    /// <param name='t_value'>
    /// T_value.
    /// </param>
    /// <param name='type'>
    /// Type.
    /// </param>
    public XmlDocument CreateReference(string t_key, string t_value, ReferenceType type = ReferenceType.normal)
    {
        List<string> lParams = new List<string> {
            "ep_cin="       + CIN,
            "ep_user="      + Username,
            "ep_entity="    + Entity,
            "ep_ref_type=auto",
            "t_value="      + t_value,
            "ep_country="   + Country,
            "ep_language="  + Language
        };

        switch (type)
        {
            case ReferenceType.boleto:
                lParams.Add("ep_type=boleto");
                break;
            case ReferenceType.recurring:
                lParams.Add("ep_rec_type=yes");
                break;
            case ReferenceType.moto:
                lParams.Add("ep_type=moto");
                break;
            default:
                break;
        }

        try
        {
            XmlDocument x = new XmlDocument();
            x.Load(getUrl(api.api_easypay_01BG, lParams));

            return x;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    /// <summary>
    /// Requests a payment.
    /// </summary>
    /// <returns>
    /// The payment.
    /// </returns>
    /// <param name='reference'>
    /// Reference.
    /// </param>
    /// <param name='key'>
    /// Key.
    /// </param>
    /// <param name='type'>
    /// Type.
    /// </param>
    //public XmlDocument RequestPayment(int reference, string key, PaymentType type = PaymentType.creditcard)
    public XmlDocument RequestPayment(int reference, string key, PaymentType type = PaymentType.creditcard)
    {
        List<string> lParams = new List<string> {
            "u=" + Username,
            "e=" + Entity,
            "r=" + reference.ToString(),
            "l=" + Language,
            "k=" + key
        };

        switch (type)
        {
            case PaymentType.recurring: lParams.Add("ep_rec_type=yes"); break;
            default: break;
        }

        try
        {
            XmlDocument x = new XmlDocument();
            x.Load(getUrl(api.api_easypay_05AG, lParams));

            return x;
        }
        catch (Exception)
        {
            return null;
        }
    }

    /// <summary>
    /// Request for the detailed payment information
    /// </summary>
    /// <returns>
    /// The payment info.
    /// </returns>
    /// <param name='ep_doc'>
    /// Ep_doc.
    /// </param>
    public XmlDocument GetPaymentInfo(string ep_doc, string ep_key)
    {
        List<string> lParams = new List<string> {
            "ep_user="  + Username,
            "ep_cin="   + CIN,
            "ep_doc="   + ep_doc
            ,"ep_key=" + ep_key
        };

        try
        {
            XmlDocument x = new XmlDocument();
            x.Load(getUrl(api.api_easypay_03AG, lParams));

            return x;
        }
        catch (Exception)
        {
            return null;
        }
    }

    /// <summary>
    /// Gets a Transaction key verification.
    /// </summary>
    /// <returns>
    /// The Transaction key verification.
    /// </returns>
    /// <param name='reference'>
    /// Reference.
    /// </param>
    public XmlDocument GetTransationVerification(int reference)
    {
        List<string> lParams = new List<string> {
            "ep_user="  + Username,
            "ep_cin="   + CIN,
            "e="        + Entity,
            "r="        + reference,
            "c="        + Country
        };

        XmlDocument x = new XmlDocument();
        x.Load(getUrl(api.api_easypay_03BG, lParams));
      
        return x;
    }

    /// <summary>
    /// Gets the API URL.
    /// </summary>
    /// <returns>
    /// The URL.
    /// </returns>
    /// <param name='x'>
    /// Api
    /// </param>
    /// <param name='p'>
    /// List of parameters
    /// </param>
    private string getUrl(api x, List<string> p = null)
    {
              
        string url = string.Format("{0}{1}.php", "http://"+ ConfigurationManager.AppSettings["EasypayUrlFinal"].ToString()+"/_s/", x);

        if (p != null)
        {            
            url += "?" + String.Join( "&" , p);
        }
        
        return url.Replace(" ", "+");
    }
}
