﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Xml;
using System.Xml.Linq;


    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class WsEasyPay : System.Web.Services.WebService
    {
        public Easypay_wrapper ep;

        [WebMethod]
        public string CreatePaymenteReference(string ValorPagamento)
        {
            string Referencia = "";
            if (ValorPagamento == "")
            {
                return Referencia;
            }
            else
            {
                DataTable dt = SubscricoesData.GerarRefMultibanco(ValorPagamento);
                if (dt.Rows.Count > 0)
                    Referencia = dt.Rows[0]["Referencia"].ToString();
                dt.Dispose();
            }
            return Referencia;
        }
    }



