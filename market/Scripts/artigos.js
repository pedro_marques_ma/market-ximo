﻿$(document).ready(function () {

});


function SubscreverItem(id) {
    $.ajax({
        type: "POST",
        url: "List.aspx/SubscreverItem",
        data: "{'id':'"+ id+ "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            if (response.d != null) {
                var i = $.parseJSON(response.d);            
                $('#btnItem' + id).css('background', '#bbbbbb');
                $('#btnItem' + id).children().css('color', '#ffffff');
                var total = $('#total_valor').attr("data-total");
                var calcTotal = (parseFloat(total) + parseFloat(i.val)).toString();
                $('#total_valor').text(parseFloat(calcTotal).toFixed(2).replace(".", ","));
                $('#total_valor').attr("data-total", (calcTotal).toString());
                $('#total_items').text(parseInt($('#total_items').text())+1);
            }              
            
        },
        failure: function (response) {           

        }
    });
}