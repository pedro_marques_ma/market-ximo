﻿$(document).ready(function () {

});


function EliminarItem(id) {
    $.ajax({
        type: "POST",
        url: "Inserir.aspx/EliminarItem",
        data: "{'id':'"+ id+ "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
        
            var i = $.parseJSON(response.d);        
            $('#' + id).remove();

            var total = $('#total').attr("data-total");
            var calcTotal = (total - i.val).toString();
            var calcSubTotal = (calcTotal - i.desc).toString();
           
            $('#subtotal').text(parseFloat(calcSubTotal).toFixed(2).replace(".", ","));
            $('#total').text(parseFloat(calcTotal).toFixed(2).replace(".", ","));
            $('#total').attr("data-total", (calcTotal).toString());
           
        },
        failure: function (response) {           

        }
    });
}

