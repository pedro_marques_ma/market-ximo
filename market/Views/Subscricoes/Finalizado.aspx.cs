﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Newtonsoft.Json.Linq;

public partial class MarketViews_Subscricoes_Finalizado : System.Web.UI.Page
{

    static decimal total_descontos = 0;
    static decimal total_sdescontos = 0;
    static decimal total_documento = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string html = "";            
           
            if (!string.IsNullOrEmpty(Request.QueryString["hash"]))
                SqlData.nserie = Request.QueryString["hash"].ToString();
            if (!string.IsNullOrEmpty(Request.QueryString["iduser"]))
                SqlData.userid = Request.QueryString["iduser"].ToString();

            SqlData.nserie = "0";
            SqlData.userid = "0";
            SqlData.idioma = "pt";

            ltl_Menu_destaque.Text = SqlData.traducao("destaque", SqlData.idioma);
            ltl_Menu_populares.Text = SqlData.traducao("populares", SqlData.idioma);
            ltl_Menu_categorias.Text = SqlData.traducao("categorias", SqlData.idioma);
            ltl_Menu_subscricoes.Text = SqlData.traducao("subscricoes", SqlData.idioma);
                                  
            lbl_titulo.Text = SqlData.traducao("obrigado_pela_sua_subscricao", SqlData.idioma);
            lbl_resumoObs_1.Text = "<p>Por favor, efetue o pagamento através da entidade e referência </p><p> Multibanco antes do prazo indicado, para finalizar a sua subscrição.</p>";
            lbl_titulo_2.Text = SqlData.traducao("dados_pagamento", SqlData.idioma);

            lbl_entidade.Text = SqlData.traducao("entidade", SqlData.idioma);
            lbl_referencia.Text = SqlData.traducao("referencia", SqlData.idioma);
            lbl_valor.Text = SqlData.traducao("valor", SqlData.idioma);

            lbl_limitePagamento.Text = SqlData.traducao("limite_de_pagamento", SqlData.idioma);

            lbl_tituloTotalItems.Text = SqlData.traducao("subscreveu_os_seguintes_items", SqlData.idioma) + " " + " (" + SubscricoesData.carrinho.listaSelecionados.Count + ")";
            html += "<table id='tb-carrinho'><tbody>";

            total_descontos = 0;
            total_sdescontos = 0;
            total_documento = 0;

            foreach (Artigo obj in SubscricoesData.carrinho.listaSelecionados)
            {
                string iconImage = "/market/images/card_default_small.png";
                if (obj.fotos.Count > 0)
                    iconImage += "/market/images/"+ obj.fotos[0].foto+"";

                html += "<tr id='"+obj.Id+"'>";
                html += "<td><div class='blockLeft' style='margin-top:20px; margin-bottom:20px;'><img src='" + iconImage +"';/></div>";
                html += "<div class='blockLeft' style='margin-left:24px; margin-top:5px;'><div class='blockLeft Cards_titulo_small' style='margin-top:40px; padding-left:5%;'>"+ obj.titulo +"</div>";
                html += "<div class='blockLeft Cards_preco_small' style='padding-left:5%; margin-top:5px;'>" + string.Format("{0:0.00}", obj.preco).Replace(".", ",") + "€ /</div></div></td>";          
                html += "</tr>";

                total_descontos += obj.preco * (obj.descontoPerc / 100);
                total_sdescontos += obj.preco;
            }
            html += "</tbody></table>";
            tblist.InnerHtml = html;

            total_documento = total_sdescontos - total_descontos;
          
        }
    }
}