﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MARKET.Master" AutoEventWireup="true" CodeFile="Inserir.aspx.cs" Inherits="MarketViews_Subscricoes_Inserir" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">   
    <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet" />
    <link href="/market/css/market.css" rel="stylesheet" /> 
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript">
    </script>
    <script src="/market/Scripts/subscricoes.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="blockLeft100 header">
            <div class="blockLeft titulo">              
                <div class="blockLeft" style="clear:left;"><asp:Label ID="lbl_breadcrumb" runat="server" CssClass="t11 cinzaA1" /></div>
            </div>
            <div class="blockLeft menu">
                <div class="menuOpcao t16"><asp:Literal ID="ltl_Menu_destaque" runat="server" /></div>
                <div class="menuOpcao t16"><asp:Literal ID="ltl_Menu_populares" runat="server" /></div>
                <div class="menuOpcao t16"><asp:Literal ID="ltl_Menu_categorias" runat="server" /></div>
                <div class="menuOpcao t16 fsemibold activo"><asp:Literal ID="ltl_Menu_subscricoes" runat="server" /></div>               
            </div>
    </div>
    <div id="pllist" style='padding-top:5%; padding-left:20%;'>   
        <div class="blockLeft"><asp:Label ID="lbl_titulo" runat="server" CssClass="subscricoesSubTitulo" style="margin-top:50px;"/></div>        
        <div id="tblist" style="margin-top:50px;" runat="server"></div>                        
        <div class="fLeft" style="width:100%; margin-top:50px;">      
            <div class="col-xs-12" id="divResumo" runat="server" style="margin-right:160px; margin-bottom:40px;">                                                         
                                 
            </div>
            <div class="fRight subscricoes_finalizar_btn" style="margin-right:150px;">   
                <asp:LinkButton Font-Underline="false" runat="server" OnClick="btn_finalizar_click"><asp:Literal ID="ltl_finalizar_btn" runat="server"></asp:Literal></asp:LinkButton>   
            </div> 
            <div class="fRight subscricoes_voltar_btn" style="margin-right:25px;">   
                <asp:LinkButton Font-Underline="false" runat="server" OnClick="btn_voltar_click"><asp:Literal ID="ltl_voltar_btn" runat="server"></asp:Literal></asp:LinkButton>            
            </div>           
        </div>                             
    </div>   
    <div class="col-lg-12 no-padding m-b" style="padding-bottom:5%;">&nbsp;</div>    
</asp:Content>
