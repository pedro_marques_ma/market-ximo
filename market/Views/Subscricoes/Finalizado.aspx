﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MARKET.Master" AutoEventWireup="true" CodeFile="Finalizado.aspx.cs" Inherits="MarketViews_Subscricoes_Finalizado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">   
    <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet" />
    <link href="/market/css/market.css" rel="stylesheet" /> 
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript">
    </script>
    <script src="/market/Scripts/subscricoes.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <div class="blockLeft100 header">
        <div class="blockLeft titulo">              
            <div class="blockLeft" style="clear:left;"><asp:Label ID="lbl_breadcrumb" runat="server" CssClass="t11 cinzaA1" /></div>
        </div>
        <div class="blockLeft menu">
            <div class="menuOpcao t16"><asp:Literal ID="ltl_Menu_destaque" runat="server" /></div>
            <div class="menuOpcao t16"><asp:Literal ID="ltl_Menu_populares" runat="server" /></div>
            <div class="menuOpcao t16"><asp:Literal ID="ltl_Menu_categorias" runat="server" /></div>
            <div class="menuOpcao t16 fsemibold activo"><asp:Literal ID="ltl_Menu_subscricoes" runat="server" /></div>               
        </div>
    </div>
    <div id="resumo" style="width:100%; margin-top:5%; padding-left:20%;">   

        <div class="fLeft" style="width:40%; margin-top:5%;">  
            <div class="fLeft" style="width:100%;"><asp:Label ID="lbl_titulo" runat="server" CssClass="subscricoesSubTitulo" style="margin-top:50px;"/></div>        
            <div class="fLeft" style="width:100%; margin-top:39px;"><asp:Label ID="lbl_resumoObs_1" runat="server" CssClass="t15w3"/></div> 
            <div class="fLeft" style="width:100%; margin-top:30px;"><asp:Label ID="lbl_titulo_2" runat="server" CssClass="t15w6"/></div>  
            <div class="fLeft t13c2D" style="width:100%; margin-top:30px;">
                <div class="fLeft"><asp:Label ID="lbl_entidade" runat="server"/></div>
                <div class="fLeft" style="margin-left:70px;"><asp:Label ID="lbl_referencia" runat="server"/></div>
                <div class="fLeft" style="margin-left:70px;"><asp:Label ID="lbl_valor" runat="server"/></div>
            </div>
            <div class="fLeft t13c2D" style="width:100%; margin-top:5px;">
                <div class="fLeft"><asp:Label ID="lbl_entidadeVal" runat="server"/>00000</div>
                <div class="fLeft" style="margin-left:70px;"><asp:Label ID="lbl_referenciaVal" runat="server"/>00000000</div>
                <div class="fLeft" style="margin-left:70px;"><asp:Label ID="lbl_valorVal" runat="server"/>00000000</div>
            </div>
            <div class="fLeft" style="width:100%; margin-top:5px;">
                <div class="fLeft t13A1" style="width:100%; margin-top:5px;"><asp:Label ID="lbl_limitePagamento" runat="server"/></div> 
                <div class="fLeft t20cA4" style="width:100%; margin-top:5px;"><asp:Label ID="lbl_limitePagamentoVal" runat="server"/>18/17/2018</div>           
            </div>  
         </div>   


         <div class="fLeft" style="width:50%; margin-top:5%;">   
            <div id="pllist" style="margin-top:5px;">   
                <div class="fLeft" style="width:100%;"><asp:Label ID="lbl_tituloTotalItems" runat="server" CssClass="subscricoesSubTitulo" style="margin-top:50px;"/></div>        
                <div id="tblist" style="margin-top:60px;" runat="server"></div>                        
                                
            </div>   
        </div>                                               
    </div> 
  
  
    <div class="col-lg-12 no-padding m-b" style="padding-bottom:5%;">&nbsp;</div>    
</asp:Content>
