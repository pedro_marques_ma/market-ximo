﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Newtonsoft.Json.Linq;

public partial class MarketViews_Subscricoes_Inserir : System.Web.UI.Page
{

    static decimal total_descontos = 0;
    static decimal total_sdescontos = 0;
    static decimal total_documento = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string html = "";            
            string filtro_pesquisa = "";
            string filtro_ordem = "1";
            string filtro_paginaIni = "1";
            string filtro_rowsPage = "12";

            if (!string.IsNullOrEmpty(Request.QueryString["hash"]))
                SqlData.nserie = Request.QueryString["hash"].ToString();
            if (!string.IsNullOrEmpty(Request.QueryString["iduser"]))
                SqlData.userid = Request.QueryString["iduser"].ToString();

            SqlData.nserie = "0";
            SqlData.userid = "0";
            SqlData.idioma = "pt";

            ltl_Menu_destaque.Text = SqlData.traducao("destaque", SqlData.idioma);
            ltl_Menu_populares.Text = SqlData.traducao("populares", SqlData.idioma);
            ltl_Menu_categorias.Text = SqlData.traducao("categorias", SqlData.idioma);
            ltl_Menu_subscricoes.Text = SqlData.traducao("subscricoes", SqlData.idioma);
           
           
            if (SubscricoesData.carrinho.listaSelecionados.Count==0)
                SubscricoesData.carrinho = SubscricoesData.EditarCarrinho(SqlData.nserie, SqlData.userid, SqlData.idioma, filtro_rowsPage, filtro_paginaIni, filtro_ordem, filtro_pesquisa);

            lbl_titulo.Text = SqlData.traducao("subscrever_items", SqlData.idioma)+" "+ " ("+ SubscricoesData.carrinho.listaSelecionados.Count +")";
            html += "<table id='tb-carrinho' width='100%'><tbody><tr><td class='first t13'>" + SqlData.traducao("subscricao", SqlData.idioma) + "</td><td class='first t13'>" + SqlData.traducao("preco_de_subscricao", SqlData.idioma) + "</td><td class='first t13'>" + SqlData.traducao("eliminar", SqlData.idioma)+ "</tr></td>";

            total_descontos = 0;
            total_sdescontos = 0;
            total_documento = 0;

            foreach (Artigo obj in SubscricoesData.carrinho.listaSelecionados)
            {
                string iconImage = "/market/images/card_default_small.png";
                if (obj.fotos.Count > 0)
                    iconImage += "/market/images/"+ obj.fotos[0].foto+"";

                html += "<tr id='"+obj.Id+"'>";
                html += "<td><div class='blockLeft' style='margin-top:20px; margin-bottom:20px;'><img src='" + iconImage +"';/></div>";
                html += "<div class='blockLeft' style='margin-left:24px; margin-top:5px;'><div class='blockLeft Cards_titulo_small' style='margin-top:40px; padding-left:5%;'>"+ obj.titulo +"</div>";
                html += "<div class='blockLeft Cards_preco_small' style='padding-left:5%; margin-top:5px;'>" + string.Format("{0:0.00}", obj.preco).Replace(".", ",") + "€ /</div></div></td>";

                html += "<td align='right' width='10%'><div class='blockLeft Cards_precoAplicar_small' style='margin-right:10px;'>" + string.Format("{0:0.00}", obj.preco).Replace(".",",") + "€</div></td>";
                html += "<td><div class='blockLeft' onclick='EliminarItem("+ obj.Id +")'><div class='blockLeft'><img src='/market/images/icon_eliminar.png'/></div><div class='blockLeft Cards_btnEliminar_small' style='margin-top:3px; padding-left:10px;'>" + SqlData.traducao("eliminar", SqlData.idioma) +"</div></div></td>";
                html += "</tr>";

                total_descontos += obj.preco * (obj.descontoPerc / 100);
                total_sdescontos += obj.preco;

            }
            html += "</tbody></table>";
            tblist.InnerHtml = html;

            total_documento = total_sdescontos - total_descontos;
            string resumoObs = "<div align='left'><p>* A renovação da subscrição dos items no Marketplace é feita aquando a renovação do seu serviço <br> X-IMO CRM. Assim os preços apresentados no ato da compra, são de acordo com o tempo em <br> falta para a renovação do seu serviço X-IMO CRM:<p></div>";
            string htmlResumo = "<table id='tb-resumo' style='width:100%;'><tbody>";
            htmlResumo += "<tr><td align='right' style='vertical-align:top;' rowspan='3' width='80%'><div class='t13 fLeft'>" + resumoObs + "</div></td><td align='right'><div class='t13 fLeft' style='margin-left:22px;'>" + SqlData.traducao("sub_total", SqlData.idioma) + " </div><div id='subtotal' class='t18'>" + string.Format("{0:0.00}", total_sdescontos).Replace(".", ",") + " €</div></td></tr>";
            htmlResumo += "<tr><td align='right'><div class='t13 fLeft' style='margin-left:22px;'>" +SqlData.traducao("descontos", SqlData.idioma) + " </div><div  id='descontos' class='t18'>" + string.Format("{0:0.00}", total_descontos).Replace(".", ",") + " €</div></td></tr>";
            htmlResumo += "<tr><td align='right'><div class='t13 fLeft' style='margin-left:22px;'>" + SqlData.traducao("total", SqlData.idioma)+ "</div><div id='total' data-total='"+ total_documento + "' class='t28'>" + string.Format("{0:0.00}", total_documento).Replace(".", ",") + " €</div></td></tr>";
            htmlResumo += "</tbody></table>";
            divResumo.InnerHtml = htmlResumo;
            
            ltl_voltar_btn.Text = "<div style='padding-left:30px; margin-top:12px;'><label class='subscricoes_voltar_btntxt'>" + SqlData.traducao("voltar_ao_marketplace", SqlData.idioma) + "</label></div>";       
            ltl_finalizar_btn.Text = "<div style='padding-left:35px; margin-top:14px;'><label class='subscricoes_finalizar_btntxt'>" + SqlData.traducao("finalizar", SqlData.idioma) + " " + SqlData.traducao("subscricao", SqlData.idioma) + "</label></div>";
          
        }
    }

    [WebMethod]
    public static string EliminarItem(string id)
    {
       
        foreach (Artigo obj in SubscricoesData.carrinho.listaSelecionados)
        {
            if (obj.Id == id)
            {
                if (SubscricoesData.EliminarItemsCarrinho(id,"0") == "1")
                {
                    SubscricoesData.carrinho.listaSelecionados.Remove(obj);
                    dynamic json = new JObject();
                    json.desc = (obj.preco * (obj.descontoPerc / 100));
                    json.val = obj.preco - (obj.preco * (obj.descontoPerc / 100));
                    total_documento = total_documento - (obj.preco - (obj.preco * (obj.descontoPerc / 100)));

                    return JsonConvert.SerializeObject(json);
                }
            }              
        }
                     
        return JsonConvert.SerializeObject("");
    }

    protected void btn_voltar_click(object sender, EventArgs e)
    {
        Response.Redirect("/market/Views/Artigos/List.aspx");
    }

    protected void btn_finalizar_click(object sender, EventArgs e)
    {

        if (total_documento > 0)
        {
            DataTable dt = new DataTable();

            /* final ok */
            //dt = SubscricoesData.GerarRefMultibanco(total_documento.ToString());

            /* teste */
            dt.Columns.Add("Entidade", Type.GetType("System.String"));
            dt.Columns.Add("Referencia", Type.GetType("System.String"));
            dt.Columns.Add("Valor", Type.GetType("System.String"));
            dt.Columns.Add("Link", Type.GetType("System.String"));

            DataRow row = dt.NewRow();
            row["Entidade"] = "00000";
            row["Referencia"] = "00000000";
            row["Valor"] = total_documento;
            row["Link"] = "";
            dt.Rows.Add(row);

            string newSubs = "";
            if(dt.Rows.Count > 0)
            {
                newSubs = SubscricoesData.InserirSubscricao(SqlData.nserie, dt.Rows[0]["Entidade"].ToString(), dt.Rows[0]["Referencia"].ToString(), dt.Rows[0]["Valor"].ToString(), dt.Rows[0]["link"].ToString(), SubscricoesData.carrinho.listaSelecionados);           
            }

            if (newSubs != "")
            {
                SubscricoesData.EliminarItemsCarrinho("0", "1");
                Response.Redirect("/market/Views/Subscricoes/Finalizado.aspx");
            }
        }
    }
}