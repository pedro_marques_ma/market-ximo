﻿<%@  Page Title="" Language="C#" MasterPageFile="~/MARKET.Master" AutoEventWireup="true" CodeFile="List.aspx.cs" Inherits="MarketViews_Artigos_List" EnableEventValidation="false"%>


<asp:Content ID="Content1" ContentPlaceHolderID="HeaderContent" runat="server">

    <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet" />
    <link href="/market/css/market.css" rel="stylesheet" /> 
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="/market/Scripts/artigos.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="blockLeft100 header">
            <div class="blockLeft titulo">
                <div class="blockLeft"><asp:Label ID="lbl_titulo" runat="server" CssClass="t22 fsemibold" /></div>
                <div class="blockLeft" style="clear:left;"><asp:Label ID="lbl_breadcrumb" runat="server" CssClass="t11 cinzaA1" /></div>
            </div>
            <div class="blockLeft menu">
                <div class="menuOpcao t16 fsemibold activo"><asp:Literal ID="ltl_Menu_destaque" runat="server" /></div>
                <div class="menuOpcao t16"><asp:Literal ID="ltl_Menu_populares" runat="server" /></div>
                <div class="menuOpcao t16"><asp:Literal ID="ltl_Menu_categorias" runat="server" /></div>
                <div class="menuOpcao t16"><asp:Literal ID="ltl_Menu_subscricoes" runat="server" /></div>  
             
                 <div class="blockRight">
                    <div class="headerCarrinho_total">                        
                            <asp:Literal ID="ltl_Menu_subsCarrinho_total" runat="server" />
                            <asp:Literal ID="ltl_Menu_subsCarrinho_totalitems" runat="server" />                       
                    </div>
                    <asp:LinkButton class="menuOpcao headerCarrinho_btn" Font-Underline="false" runat="server" OnClick="btn_subscreverItems_click"><asp:Literal ID="ltl_Menu_subsCarrinho_btn" runat="server"></asp:Literal></asp:LinkButton>          
                </div>
                 
            </div>
    </div>
    <div id="pllist" style='padding-top:5%; padding-left:20%;'>   
        <div id="tblist" runat="server"></div>
    </div> 
    <div class="col-lg-12 no-padding m-b" style="padding-bottom:5%;">&nbsp;</div>    
</asp:Content>