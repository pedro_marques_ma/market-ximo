﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;



public partial class MarketViews_Artigos_List : System.Web.UI.Page
{

    

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            string html = "";
            string filtro_listaTipo = "1";
            string filtro_pesquisa = "";
            string filtro_ordem = "1";
            string filtro_ativo = "1";
            string filtro_paginaIni = "1";
            string filtro_rowsMax = "12";

            if (!string.IsNullOrEmpty(Request.QueryString["hash"]))
                SqlData.nserie = Request.QueryString["hash"].ToString();
            if (!string.IsNullOrEmpty(Request.QueryString["iduser"]))
                SqlData.userid = Request.QueryString["iduser"].ToString();

            SqlData.nserie = "0";
            SqlData.userid = "0";
            SqlData.idioma = "pt";

            if (SqlData.nserie != null)
                SqlData.setConnString();

            ltl_Menu_destaque.Text = SqlData.traducao("destaque", SqlData.idioma);
            ltl_Menu_populares.Text = SqlData.traducao("populares", SqlData.idioma);
            ltl_Menu_categorias.Text = SqlData.traducao("categorias", SqlData.idioma);
            ltl_Menu_subscricoes.Text = SqlData.traducao("subscricoes", SqlData.idioma);
            

            ArtigosData.lista = new List<Artigo>();
            ArtigosData.lista = ArtigosData.ListarArtigos(filtro_listaTipo, SqlData.idioma, filtro_rowsMax, filtro_paginaIni, filtro_ordem, filtro_pesquisa, filtro_ativo);

            SubscricoesData.carrinho = new Carrinho();
            SubscricoesData.carrinho.listaSelecionados = new List<Artigo>();
          
            decimal total_carrinhoValor = 0;
            int total_carrinhoItems = 0;

            string categoriaId = "";
            int categoriaCount = 1;

            html += "<table id='tb-artigos' border='0'>";

            foreach (Artigo obj in ArtigosData.lista)
            {
                if (categoriaId == "" && categoriaCount == 1)
                {
                    html += "<tr class='Cards_categoria'><td id='" + categoriaId + "'><table><tr><td style='padding-top: 50px; padding-bottom: 10px;'>" + obj.categoriaNome.ToString() + "</td></tr></table></td></tr>";
                    categoriaCount = 2;
                    categoriaId = obj.categoriaId.ToString();
                }

                if (categoriaId == obj.categoriaId.ToString())
                {
                    html += "<td><div class='Cards'><table id='" + obj.Id + "'>";

                    if (obj.fotos.Count > 0)
                        html += "<tr><td><img src='/market/images/" + obj.fotos[0].foto + "'/></td></tr>";
                    else
                        html += "<tr><td><img src='/market/images/card_default.png'></td></tr>";

                    html += "<tr class='Cards_titulo'><td>" + obj.titulo + "</td></tr>";
                    html += "<tr class='Cards_periocidade'><td style='padding-top: 5px; padding-bottom: 5px;'>" + obj.periocidade + "</td></tr>";
                    html += "<tr class='Cards_preco'><td style='padding-top: 10px; padding-bottom: 20px;'>" + obj.preco + "</td></tr>";

                    if (obj.existeCarrinho == 1)
                    {
                        SubscricoesData.carrinho.listaSelecionados.Add(obj);
                        html += "<tr><td><div id='btnItem" + obj.Id + "' class='Cards_btn' style='background:#bbbbbb;' onclick='SubscreverItem(" + obj.Id + ")'><div class='Cards_btntxt' style='color:#ffffff;'>" + SqlData.traducao("subscrever", SqlData.idioma) + "</div></div></td></tr>";
                        total_carrinhoItems++;
                        total_carrinhoValor += obj.preco;
                    }
                    else
                        html += "<tr><td><div id='btnItem" + obj.Id + "' class='Cards_btn' onclick='SubscreverItem(" + obj.Id + ")'><div class='Cards_btntxt'>" + SqlData.traducao("subscrever", SqlData.idioma) + "</div></div></td></tr>";

                    html += "</table></div></td>";
                    categoriaId = obj.categoriaId.ToString();


                }
                else
                {
                    html += "<tr class='Cards_categoria'><td id='" + categoriaId + "'><table><tr><td style='padding-top: 50px; padding-bottom: 10px;'>" + obj.categoriaNome.ToString() + "</td></tr></table></td></tr>";

                    html += "<td><div class='Cards'><table id='" + obj.Id + "'>";

                    if (obj.fotos.Count > 0)
                        html += "<tr><td><img src='/market/images/" + obj.fotos[0].foto + "'/></td></tr>";
                    else
                        html += "<tr><td><img src='/market/images/card_default.png'></td></tr>";

                    html += "<tr class='Cards_titulo'><td>" + obj.titulo + "</td></tr>";
                    html += "<tr class='Cards_periocidade'><td style='padding-top: 5px; padding-bottom: 5px;'>" + obj.periocidade + "</td></tr>";
                    html += "<tr class='Cards_preco'><td style='padding-top: 10px; padding-bottom: 20px;'>" + obj.preco + "</td></tr>";

                    if (obj.existeCarrinho == 1)
                    {
                        SubscricoesData.carrinho.listaSelecionados.Add(obj);
                        html += "<tr><td><div id='btnItem" + obj.Id + "' class='Cards_btn' style='background:#bbbbbb;' onclick='SubscreverItem(" + obj.Id + ")'><div class='Cards_btntxt' style='color:#ffffff;'>" + SqlData.traducao("subscrever", SqlData.idioma) + "</div></div></td></tr>";
                        total_carrinhoItems ++; 
                        total_carrinhoValor += obj.preco;
                    }
                    else
                        html += "<tr><td><div id='btnItem" + obj.Id + "' class='Cards_btn' onclick='SubscreverItem(" + obj.Id + ")'><div class='Cards_btntxt'>" + SqlData.traducao("subscrever", SqlData.idioma) + "</div></div></td></tr>";

                    html += "</table></div></td>";
                    categoriaId = obj.categoriaId.ToString();
                }
            }
            html += "</table>";

            ltl_Menu_subsCarrinho_total.Text = "<label id='total_valor' data-total='" + total_carrinhoValor + "' class='headerCarrinho_totaltxt' style='margin-left:10px;'>" + string.Format("{0:0.00}", total_carrinhoValor).Replace(".", ",") + "€</label>";
            ltl_Menu_subsCarrinho_totalitems.Text = "<div class='headerCarrinho_totalitems'><label id='total_items' class='headerCarrinho_totalitemstxt'>" + total_carrinhoItems + "</label></div>";
            ltl_Menu_subsCarrinho_btn.Text = "<label class='headerCarrinho_btntxt'>" + SqlData.traducao("subscrever_items", SqlData.idioma) + "</label>";

            tblist.InnerHtml = html;
        }
    }

    protected void btn_subscreverItems_click(object sender, EventArgs e)
    {

        Response.Redirect("/market/Views/Subscricoes/Inserir.aspx");
    }

    [WebMethod]
    public static string SubscreverItem(string id)
    {

        if (SubscricoesData.carrinho.listaSelecionados.Exists(el => el.Id == id))
        {
            return ("0");
        }
        else
        {
            foreach (Artigo obj in ArtigosData.lista)
            {
                if (obj.Id == id)
                {
                    SubscricoesData.carrinho.listaSelecionados.Add(obj);
                    SubscricoesData.InserirCarrinhoItem(id);

                    dynamic json = new JObject();
                    json.desc = (obj.preco * (obj.descontoPerc / 100));
                    json.val = obj.preco - (obj.preco * (obj.descontoPerc / 100));

                    return JsonConvert.SerializeObject(json);
                }
            }

            return ("");
        }        
    }
}


